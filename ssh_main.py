
import paramiko
import json
import socket

with open("SshSecret",'r') as credentials:
   dict_cred=json.load(credentials)


class ssh_conn:
      def __init__(self,username,password,server,command):
          self.username=username
          self.password=password
          self.server=server
          self.look_for_keys=False
          self.command=command
      def establish(self):
          self.conn_flag = False
          try:
              self.client = paramiko.SSHClient()
              self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
              self.client.connect(self.server,username=self.username,password=self.password,look_for_keys=self.look_for_keys)
              print "--------------------------------------------------------"
              print ("ssh client connection established by user {} to {} ").format(self.username,self.server)
              self.conn_flag = True
          except paramiko.AuthenticationException:
              print "Authentication failed, please verify your credentials"
          except paramiko.SSHException as sshException:
             print "Could not establish SSH connection: %s" % sshException
          except socket.timeout as e:
            print "Connection timed out"
          except Exception as e:
            print '\nException in connecting to the server',e
            self.client.close()
          return self.conn_flag
      def execute_cmd(self):
          try:
              if self.establish():
                  ssh_in,ssh_out,ssh_err=self.client.exec_command(self.command)
                  self.ssh_out=ssh_out.read()
                  self.ssh_err=ssh_err.read()
                  if len(self.ssh_err) > 0:
                         self.client.close()
                         return self.ssh_err
                  print "Command execution completed successfully",self.command
                  self.client.close()
                  return self.ssh_out
              else:
                  print "Couldn't establish ssh connection"
                  return "unknown"
          except Exception as err:
              print 'exceute_cmd method excption:', err


if __name__ == "__main__" :
    result_dict={"unknown":0}
    for ip in dict_cred["server"]:
        connection = ssh_conn(username=dict_cred["username"],password=dict_cred["password"],server=ip,command="cat /etc/redhat-release")
        command_output=connection.execute_cmd()
        print command_output
        print "--------------------------------------------------------"
        if command_output=="unknown":
            result_dict[command_output]+=1
        elif command_output.split()[-2].split('.')[0] in result_dict.keys():
           result_dict[command_output.split()[-2].split('.')[0]]+= 1
        else:
           result_dict[command_output.split()[-2].split('.')[0]]=1
    print result_dict