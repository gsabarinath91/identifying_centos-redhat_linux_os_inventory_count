package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"io/ioutil"
	"golang.org/x/crypto/ssh"
)

type conninfo struct {
	Username string
	Password string
	Server   []string
}

func ssh_conn(username string, password string, server string, results chan <-string,err_chn chan <- error) {
	conn_config := &ssh.ClientConfig{
		User: username,
		Auth: []ssh.AuthMethod{
			ssh.Password(password),
		},
	}
	conn_config.HostKeyCallback = ssh.InsecureIgnoreHostKey()
	ssh_client, err := ssh.Dial("tcp", server, conn_config)
	if err != nil {
		results <-"unknown"
		fmt.Println("Failed to connect :",server)
		err_chn <- err
	}else {
		ssh_session, err := ssh_client.NewSession()
		if err != nil {
			ssh_client.Close()
		}else{
			fmt.Printf("ssh client connection established by user {%s} to {%s}\n", username, server)
	        command_output, err := ssh_session.CombinedOutput("cat /etc/redhat-release")
	        if err != nil {
		          log.Fatal(err)
	        }
		results <- string(command_output)
		defer ssh_client.Close()
		}
	}
}

func main() {
	var info conninfo
	file, _ := os.Open("SshConnect.json")
	cred, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}
	json.Unmarshal(cred, &info)
	username := info.Username
	password := info.Password
	server := info.Server
	results := make(chan string, 10)
	err_chn := make(chan error)

	for _, ip := range server {
		go ssh_conn(username, password, ip+":22", results,err_chn)
	}
	result_map := map[string]int{}
	result_map["unknown"] = 0
	for i := 0; i < len(server); i++ {
		key := <-results
		switch {
		case key == "unknown":
			result_map["unknown"] += 1
		case key != "unknown":	
			osver := strings.Split(strings.TrimSpace(key), " ")
			osver_key := strings.Split(osver[len(osver)-2], ".")[0]
			if count, exist := result_map[osver_key]; exist {
				result_map[osver_key] = count + 1
			} else {
			result_map[osver_key] = 1
			}
	   }  
	}

	for version, count := range result_map {
		fmt.Println("OS version "+version+"=", count)
	}
	defer file.Close()
}
