# README #

https://geekcontainer.wordpress.com/2018/05/29/code-to-identify-a-count-of-centos-redhat-operating-system-in-large-inventry/

### What is this repository for? ###

* Quick summary
	
	A Tiny version of code to find the CentOs/RedhatOS version in whole bunch of server running in your inventory.
	I have coded in two language python and GO ,both will use their inbuilt libarary of ssh to connect to remote host and find the Operating system version and parse it for count.
	The final outcome would be a dictionary data type in python and string statement in Go.

* Version
1.0
* g.sabarinath91@gmail.com

### How do I get set up? ###

* Summary of set up
   
   The repository contains four files with set of two pairs
   		1)SshConnect.Json & ssh_main.go
		2)SshSecret & ssh_mai.py
	The common username and password for servers are feeded in json file along with list of servers.

* Configuration
     
	 Go code alone use a concurrency model to establish multiple ssh connection using thread pool by invoking goroutine and channel.Whereas python use iterrative fashion to each server at a time.
* Deployment instructions
     
	 git clone https://gsabarinath91@bitbucket.org/gsabarinath91/identifying_centos-redhat_linux_os_inventory_count.git
     
	 python ssh_main.py
	 
	 go run ssh_main.go
	 
### Contribution guidelines ###
* Areas to improves:
     Need benchmark running against large inventory.
	 Implementation of concurrency in python 
	 Better Error Handling in GO and Python
	 Formating Output

### Who do I talk to? ###

* Repo owner or admin
g.sabarinath91@gmail.com